
let profil = {}
let numeroQuestion = 0 
let resultaQuiz= []
let score = 0
let ScoreUtilisateur = 0
let dialog
$(function(){
    $("#formbutton").on('click',function(){
      formulaireValidations()
    })

    /**********Validation formulaire ************** */

  function formulaireValidations(){
      $("#formaValidation").validate({
        rules: {
          nom:{
            required: true,
            alphanumeric:true
          },
          prenom:{
            required: true,
            alphanumeric:true
          },
        date:{
          required: true,
          datePlusPetite: true
        },
        choix: {
          required: true
        }
      },
        messages: {
          prenom: "Veuillez entrer un prénom",
          nom: "Veuillez entrer un nom",
          date: "Veuillez entrer votre date de naissance",
          choix: "Veuillez entrer votre statut"
        },
        submitHandler: function(form) {
          profil.nom = $('#nom').val()
          profil.prenom = $('#prenom').val()
          profil.statut = $('#choix').val()
          profil.dateNaissance = $('#dateNaissance').val()
          $("#formaValidation").remove()
          afficheProfile(profil)
          AficherText()
          creationQuestion(question)
        },
          showErrors: function(errorMap, errorList) {
                  var sommaire = "Vous avez les erreurs: \n"
                  const ul = $('<ul></ul>')
                  $.each(errorList, function() {ul.append(`<li>${this.message}</li>`) });
                   $('.listeDeserreurs').html(ul)
                   $('.alert').show()
                  this.defaultShowErrors();
          },
          invalidHandler: function(form, validator) {
            est_soumis = true;
         }
      });
  }
  
  jQuery.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[\w.]+$/i.test(value);
  }, "Letters, numbers, and underscores only please");
  
  jQuery.validator.addMethod('datePlusPetite', function(value, element){
    let dateActuelle = new Date()
    return this.optional(element) || dateActuelle >= new Date(value)
  }, "la date de naissance doit être inférieur à la date d'aujourd'hui")
  
  /*************données sous format JSON****************/
  
  let questions = `
   [
     {
       "question":"Que veut dire twillight en anglais ?",
       "reponses":[
       "Crépuscule",
       "Vampire", 
       "Loup garou"
       ], 
       "reponse":0
     },
     {
       "question":"Que veut dire mdr ?",
       "reponses":[
         "Mort de rire",
         "La peur des acarien", 
         "Moindre",
         "Je ne sais pas"
       ], 
       "reponse":0
     },
     {
      "question":"Le titanic a fêté ses 100 ans ?",
      "reponses":[
      "Vrai",
      "Faut", 
      "IL n'a pas coulé"
      ], 
      "reponse":0
    },
    {
      "question":"Qu'est ce que une rose ?",
      "reponses":[
      "La vie",
      "Une fleur",
      "Je ne sais pas"
      ], 
      "reponse":1
    }
   ]
   `
   let question = JSON.parse(questions)

/***********afficher Quiz************/

  function creationQuestion(question) {
    afficahgeQuestionReponse()
      $('#afficheResultat').on('click', function(){
          afficheModalScoreQuiz() 
      })
  }


/***************Permet la création du question avec les réponses  */

function afficahgeQuestionReponse(){
  let maquestion = question[numeroQuestion].question
    $('.questionaire').html( '<h2>' +  (maquestion) + '</h2>')
      for(let j = 0; j < question[numeroQuestion].reponses.length; j++ ){
        $('.questionaire').append('<p>'+ `<label> <input type="radio" name="radio" id="rdiobuttontext" value=${j}>` + " " + `${question[numeroQuestion].reponses[j]}</label>` + '</p>')
      }
      $('.questionaire').append('</br>'+ '<button class="btn btn-primary" id="questionSuivante">Question suivante </button>')
      numeroQuestion++
      $('.questionaire').addClass('questionaireShow')
      questionSuivante()
      if ( numeroQuestion == question.length){
        $('.questionaire').append('<button class="btn btn-primary" id="afficheResultat"> Résulta Quiz</button>')
      $('#questionSuivante').prop("disabled",true);
      }
}
      
/*******Pour passer à la question suivante ************/

  function questionSuivante() {
    $('#questionSuivante').on('click', function() {
        const reponseQuiz = $("input[name='radio']:checked").val()
        $('#questionSuivante').addClass('classetransition')
        resultaQuiz.push(reponseQuiz)
        creationQuestion(question)
    })
  }

  /*********** Calcule Age************/ 

  function getAge (profil) {
    const dateDeAjourdhui = new Date()
    const dateNaissance = new Date(profil.dateNaissance)
    let age = dateDeAjourdhui.getFullYear() - dateNaissance.getFullYear()
    const m = dateDeAjourdhui.getMonth() - dateNaissance.getMonth()
    if (m < 0 || (m === 0 && dateDeAjourdhui.getDate() < dateNaissance.getDate())) {
      age--
    }
    return age
  }

   /**********Modal jquery UI ****************/

  dialog = $( "#dialog-message" ).dialog({
    autoOpen: false,
    width: 350,
    height: 300,
    modal: true,
    hide: {
      effect: "explode",
      duration: 1000
    },
    buttons: {
      "Voir les détailles": voirDetailles
    },
  })
  $('#dialog-message').dialog('option', 'position', 'center')


  /*********Calcule le score et faitappel à la fonction afficheModal()**********/

  function afficheModalScoreQuiz() {
    const reponseQuiz = $("input[name='radio']:checked").val()
    resultaQuiz.push(reponseQuiz)
    for(let i = 0; i < resultaQuiz.length; i++){
      if (resultaQuiz[i] == question[i].reponse){
          score++
      }
    }
    ScoreUtilisateur = (score/resultaQuiz.length)*100
    afficherModal()
  }

/*************Permet d'affiche la modal avec le score (réussis ou échec) ********* */

function afficherModal() {

  $("#dialog-message").dialog("open")
  $('#dialog-message').append(`<p><b> Vous avez eu un scroe de : ${ScoreUtilisateur} % </p></b>`)
  if(ScoreUtilisateur >= "50"){
    $('#dialog-message').append('<span class="colorResultat"><b>Bravo Quiz réussis </b></span>')
    $('.colorResultat').css( "color", "green")
    }else{
      $('#dialog-message').append('<span class="colorResultat"><b>Désolé vous avez échoué le Quiz</b><span>')
      $('.colorResultat').css( "color", "red")
    }
}

/****Affiche les détailles des résulats avec un accordiant et Datatable et le profil de l'utilisateur **********/

function voirDetailles(){
  const accordion = $('#accordion')
  for (let i = 0; i < question.length; i++){
     $('<h2>' + ' ' + (i + 1) + '-' + ' ' + question[i].question + '</h2>').appendTo(accordion)
     const div = $('<div></div>')
     for(let j = 0; j < question[i].reponses.length; j++){
       $('<p>'+ '. ' + question[i].reponses[j] + '</p>').appendTo(div)
     }
     div.appendTo(accordion)
  } 
  $("#accordion").accordion()
  $('.questionaire').remove()
  afficheTable (question)
  dialog.dialog( "close" )
}

/****Permet l'affichage du Dtatatable avec les résultats su quiz*******************/

function afficheTable (question) {

    $('thead').append('<tr>' + '<th>Numéro</th>' + '<th>Question</th>' + '<th>Résultat</th>' + '</tr>' )
    for (let i = 0; i < question.length; i++) {
      const tr = $('<tr></tr>')
      tr.append('<td>' + (i + 1) + '</td>')  // le +1 est pour ne pas avoir de question #0 et les parenthèses sont obligatoires.
      tr.append('<td>' + question[i].question + '</td>')
      if (resultaQuiz[i] == question[i].reponse) {
        tr.append('<td><i class="fas fa-check iconecheck"></i></td>')
      } else {
        tr.append('<td><i class="fas fa-times iconecroix"></i></td>')
      }
      $('.iconecheck').css('color', 'green')
      $('.iconecroix').css('color', 'red')
      $('tbody').append(tr)
      $('#datatable').DataTable()
    }
  }

function afficheProfile(profil) {
  $('#imageProfil').prepend('<img class="inline" src="../img/image-profil.png">');
  $('#profil').append('<p>' + '<b> Nom : </b>' +  profil.nom + '</p>')
  $('#profil').append('<p>' + '<b> Prénom : </b>' +profil.prenom + '</p>')
  $('#profil').append('<p>' + '<b> Âge :  </b>' + getAge(profil) + ' ' +'ans' + '</p>')
  $('#profil').append('<p>' + '<b> Statut :  </b>' +profil.statut + '</p>')
  $('.ajoutLigne').append('<hr>')
  $('hr').addClass('stylehr')
}
/*******Affiche le mot de bienvenue******************/

function AficherText() {
  $('#bienVenue').append('<h2> Bienvenue!</h2>')
  $('#bienVenue').append('<strong><h3>Testez vos connaissnaces générales</h3></strong>')
  $('#bienVenue').addClass('motBienvenue')
}

})
